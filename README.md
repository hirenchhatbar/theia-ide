# theia-ide

https://theia-ide.org/
https://theia-ide.org/docs/composing_applications/

# How to start
```
git clone https://github.com/eclipse-theia/theia.git /web/theia-ide.managemepro.com
cd /web/theia-ide.managemepro.com/examples/browser
yarn theia start /web/design.clearlyinteriors.managemepro.com --hostname 0.0.0.0 --port 8080
```
